
package testdatadownload;

/**
 *
 * @author david.ambros
 */
public interface Osservatore {
	public void update();
	public void setOsservabile(Osservabile osservabile);
}
